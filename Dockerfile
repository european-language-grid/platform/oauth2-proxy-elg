ARG OAUTH2_PROXY_VERSION=7.1.2

FROM quay.io/oauth2-proxy/oauth2-proxy:v${OAUTH2_PROXY_VERSION}

ARG YQ_VERSION=4.9.3
# Switch to root for write access to /bin
USER root
RUN wget -O /bin/yq https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_linux_amd64 && chmod +x /bin/yq
USER 2000:2000

COPY entrypoint.sh /bin/entrypoint

ENTRYPOINT ["/bin/entrypoint"]
