Proxy for X-Original-Method
===========================

When `ingress-nginx` calls the `auth-url` for an ingress, it always makes a GET request to the auth proxy, but it includes the HTTP method of the original request as the `X-Original-Method` header.  This is a simple reverse proxy that detects `X-Original-Method: OPTIONS` and converts the method of the upstream request from GET to OPTIONS to match.  This allows oauth2-proxy's `skip_auth_preflight` to work correctly, permitting CORS preflight requests (which do not themselves carry an `Authorization` header) to pass through to the real backend.
