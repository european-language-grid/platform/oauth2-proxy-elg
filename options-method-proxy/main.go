package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
)

var listenAddress string
var upstream string

var reverseProxy *httputil.ReverseProxy

func init() {
	flag.StringVar(&listenAddress, "l", ":8081", "Local address (:port or ip:port) to listen on")
	flag.StringVar(&upstream, "u", "http://127.0.0.1:4180", "Upstream server address to which we will proxy")
}

func main() {
	flag.Parse()
	upstreamUrl, err := url.Parse(upstream)
	if err != nil {
		panic(fmt.Sprintf("%s is not a valid URL", upstream))
	}

	log.Printf("Proxying to upstream at %s", upstream)

	reverseProxy = httputil.NewSingleHostReverseProxy(upstreamUrl)
	log.Printf("Listening on %s", listenAddress)
	log.Fatal(http.ListenAndServe(listenAddress, http.HandlerFunc(handleOptions)))
}

func handleOptions(w http.ResponseWriter, r * http.Request) {
	origMethod := r.Header.Get("X-Original-Method")
	if origMethod == "OPTIONS" {
		r.Method = "OPTIONS"
	}

	reverseProxy.ServeHTTP(w, r)
}
