#!/bin/sh

mkdir -p /tmp/oauth2-proxy

# This weird looking construct finds all .yaml files anywhere under
# /elg/config, and sorts them lexicographically by their basename regardless
# of directory, e.g. given
#
# /elg/config/base/10-first.yaml
# /elg/config/base/99-last.yaml
# /elg/config/extra/50-middle.yaml
#
# FILES will end up in the order 10-first, 50-middle, 99-last.  This matters
# because we merge the files in order, with entries in later files overriding
# entries in earlier files.
FILES=`find /elg/config -name '*.yaml' | grep -v '\.\.' | sed 's/\(.*\)\/\([^\/]*\)$/\2\/\1\/\2/' | sort | sed 's/^[^\/]*\///'`
echo "Processing config files:"
echo $FILES

# Merge files in lexicographic order, then de-duplicate the
# injectResponseHeaders so that only the last setting for each header name is
# included.
/bin/yq eval-all '. as $item ireduce({}; . *+ $item)' $FILES | \
    /bin/yq eval '.injectResponseHeaders |= (.[] | (. as $item ireduce([]; [$item] + .)) | unique_by(.name))' - \
    > /tmp/oauth2-proxy/alpha-config.yaml

# Run the proxy with our constructed YAML config
exec /bin/oauth2-proxy --alpha-config=/tmp/oauth2-proxy/alpha-config.yaml "$@"
